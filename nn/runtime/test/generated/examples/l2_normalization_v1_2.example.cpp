// clang-format off
// Generated file (from: l2_normalization_v1_2.mod.py). Do not edit
std::vector<MixedTypedExample>& get_examples_dim1_axis0() {
static std::vector<MixedTypedExample> examples_dim1_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_dim1_axis0;
};

std::vector<MixedTypedExample>& get_examples_dim1_axis0_float16() {
static std::vector<MixedTypedExample> examples_dim1_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_dim1_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_dim2_axis1() {
static std::vector<MixedTypedExample> examples_dim2_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_dim2_axis1;
};

std::vector<MixedTypedExample>& get_examples_dim2_axis1_float16() {
static std::vector<MixedTypedExample> examples_dim2_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_dim2_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_dim3_axis2() {
static std::vector<MixedTypedExample> examples_dim3_axis2 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_dim3_axis2;
};

std::vector<MixedTypedExample>& get_examples_dim3_axis2_float16() {
static std::vector<MixedTypedExample> examples_dim3_axis2_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_dim3_axis2_float16;
};

std::vector<MixedTypedExample>& get_examples_relaxed_dim1_axis0() {
static std::vector<MixedTypedExample> examples_relaxed_dim1_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_relaxed_dim1_axis0;
};

std::vector<MixedTypedExample>& get_examples_relaxed_dim1_axis0_float16() {
static std::vector<MixedTypedExample> examples_relaxed_dim1_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_relaxed_dim1_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_relaxed_dim2_axis1() {
static std::vector<MixedTypedExample> examples_relaxed_dim2_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_relaxed_dim2_axis1;
};

std::vector<MixedTypedExample>& get_examples_relaxed_dim2_axis1_float16() {
static std::vector<MixedTypedExample> examples_relaxed_dim2_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_relaxed_dim2_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_relaxed_dim3_axis2() {
static std::vector<MixedTypedExample> examples_relaxed_dim3_axis2 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_relaxed_dim3_axis2;
};

std::vector<MixedTypedExample>& get_examples_relaxed_dim3_axis2_float16() {
static std::vector<MixedTypedExample> examples_relaxed_dim3_axis2_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_relaxed_dim3_axis2_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis0() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.36f, 0.48f, 0.8f, 0.64f, 0.6f, 0.0f, 0.6f, 0.0f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.8f, 0.0f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.36f, 0.48f, 0.8f, 0.64f, 0.6f, 0.0f, 0.6f, 0.0f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.8f, 0.0f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis1() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.64f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis1;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis1_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis1_neg() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis1_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.64f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis1_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis1_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis1_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis1_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis2() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis2 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f, 0.36f, 0.48f, 0.48f, 0.6f, 0.8f, 0.64f, 0.8f, 0.64f, 0.36f, 0.6f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis2;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis2_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis2_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.47999998927116394f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.800000011920929f, 0.6399999856948853f, 0.36000001430511475f, 0.6000000238418579f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis2_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis2_neg() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis2_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f, 0.36f, 0.48f, 0.48f, 0.6f, 0.8f, 0.64f, 0.8f, 0.64f, 0.36f, 0.6f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis2_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis2_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis2_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.47999998927116394f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.800000011920929f, 0.6399999856948853f, 0.36000001430511475f, 0.6000000238418579f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis2_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis3() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis3 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.48f, 0.6f, 0.64f, 0.8f, 0.36f, 0.48f, 0.64f, 0.6f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis3;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis3_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis3_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.47999998927116394f, 0.6000000238418579f, 0.6399999856948853f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.6399999856948853f, 0.6000000238418579f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis3_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis3_neg() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis3_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.48f, 0.6f, 0.64f, 0.8f, 0.36f, 0.48f, 0.64f, 0.6f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim4_axis3_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim4_axis3_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim4_axis3_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.47999998927116394f, 0.6000000238418579f, 0.6399999856948853f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.6399999856948853f, 0.6000000238418579f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_dim4_axis3_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis0() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim3_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_dim3_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim3_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_dim3_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis1() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim3_axis1;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis1_float16() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_dim3_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis1_neg() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis1_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim3_axis1_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis1_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis1_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_dim3_axis1_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis2() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis2 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim3_axis2;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis2_float16() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis2_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_dim3_axis2_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis2_neg() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis2_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim3_axis2_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim3_axis2_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim3_axis2_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_dim3_axis2_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis0() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim2_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_dim2_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim2_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_dim2_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis1() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim2_axis1;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis1_float16() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_dim2_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis1_neg() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis1_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim2_axis1_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim2_axis1_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim2_axis1_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_dim2_axis1_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim1_axis0() {
static std::vector<MixedTypedExample> examples_axis_dim1_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim1_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_dim1_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_dim1_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_dim1_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_dim1_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_dim1_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_dim1_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_dim1_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_dim1_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_dim1_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis0() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.36f, 0.48f, 0.8f, 0.64f, 0.6f, 0.0f, 0.6f, 0.0f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.8f, 0.0f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.36f, 0.48f, 0.8f, 0.64f, 0.6f, 0.0f, 0.6f, 0.0f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.8f, 0.0f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 9.0f, 12.0f, 20.0f, 16.0f, 3.0f, 0.0f, 6.0f, 0.0f, 12.0f, 15.0f, 9.0f, 15.0f, 4.0f, 4.0f, 0.0f, 9.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis1() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.64f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis1;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis1_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis1_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis1_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.64f, 0.48f, 0.6f, 0.36f, 0.6f, 0.8f, 0.64f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis1_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis1_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis1_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 16.0f, 12.0f, 15.0f, 9.0f, 15.0f, 20.0f, 16.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.6000000238418579f, 0.36000001430511475f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis1_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis2() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis2 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f, 0.36f, 0.48f, 0.48f, 0.6f, 0.8f, 0.64f, 0.8f, 0.64f, 0.36f, 0.6f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis2;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis2_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis2_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.47999998927116394f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.800000011920929f, 0.6399999856948853f, 0.36000001430511475f, 0.6000000238418579f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis2_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis2_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis2_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f, 0.36f, 0.48f, 0.48f, 0.6f, 0.8f, 0.64f, 0.8f, 0.64f, 0.36f, 0.6f, 0.48f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis2_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis2_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis2_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f, 9.0f, 12.0f, 12.0f, 15.0f, 20.0f, 16.0f, 20.0f, 16.0f, 9.0f, 15.0f, 12.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.47999998927116394f, 0.6000000238418579f, 0.800000011920929f, 0.6399999856948853f, 0.800000011920929f, 0.6399999856948853f, 0.36000001430511475f, 0.6000000238418579f, 0.47999998927116394f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis2_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis3() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis3 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.48f, 0.6f, 0.64f, 0.8f, 0.36f, 0.48f, 0.64f, 0.6f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis3;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis3_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis3_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.47999998927116394f, 0.6000000238418579f, 0.6399999856948853f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.6399999856948853f, 0.6000000238418579f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis3_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis3_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis3_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f, 0.36f, 0.48f, 0.8f, 0.48f, 0.6f, 0.64f, 0.8f, 0.36f, 0.48f, 0.64f, 0.6f, 0.48f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis3_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim4_axis3_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim4_axis3_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f, 9.0f, 12.0f, 20.0f, 12.0f, 15.0f, 16.0f, 20.0f, 9.0f, 12.0f, 16.0f, 15.0f, 12.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f, 0.36000001430511475f, 0.47999998927116394f, 0.800000011920929f, 0.47999998927116394f, 0.6000000238418579f, 0.6399999856948853f, 0.800000011920929f, 0.36000001430511475f, 0.47999998927116394f, 0.6399999856948853f, 0.6000000238418579f, 0.47999998927116394f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim4_axis3_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis0() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.8f, 0.6f, 0.0f, 0.6f, 0.0f, 0.8f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 8.0f, 12.0f, 3.0f, 0.0f, 6.0f, 0.0f, 4.0f, 4.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis1() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis1;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis1_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis1_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis1_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f, 0.8f, 0.8f, 0.6f, 0.0f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis1_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis1_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis1_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f, 8.0f, 12.0f, 6.0f, 0.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis1_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis2() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis2 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis2;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis2_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis2_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis2_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis2_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis2_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f, 0.8f, 0.6f, 0.0f, 0.8f, 0.0f, 0.6f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis2_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim3_axis2_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim3_axis2_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f, 8.0f, 6.0f, 0.0f, 12.0f, 0.0f, 9.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.0f, 0.6000000238418579f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim3_axis2_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis0() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.6f, 0.0f, 0.8f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 3.0f, 0.0f, 4.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.6000000238418579f, 0.0f, 0.800000011920929f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis0_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis1() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis1 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis1;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis1_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis1_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis1_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis1_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis1_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f, 0.6f, 0.0f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis1_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim2_axis1_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim2_axis1_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f, 3.0f, 0.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f, 0.6000000238418579f, 0.0f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim2_axis1_neg_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim1_axis0() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim1_axis0 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim1_axis0;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim1_axis0_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim1_axis0_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim1_axis0_float16;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim1_axis0_neg() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim1_axis0_neg = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 3.0f, 4.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.0f, 0.6f, 0.8f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim1_axis0_neg;
};

std::vector<MixedTypedExample>& get_examples_axis_relaxed_dim1_axis0_neg_float16() {
static std::vector<MixedTypedExample> examples_axis_relaxed_dim1_axis0_neg_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 3.0f, 4.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.0f, 0.6000000238418579f, 0.800000011920929f}}},
}
},
}, // End of an example
};
return examples_axis_relaxed_dim1_axis0_neg_float16;
};

