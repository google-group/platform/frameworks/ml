// clang-format off
// Generated file (from: minimum.mod.py). Do not edit
std::vector<MixedTypedExample>& get_examples_simple() {
static std::vector<MixedTypedExample> examples_simple = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.0f, 0.0f, -1.0f, 11.0f, -2.0f, -1.44f}}, {1, {-1.0f, 0.0f, 1.0f, 12.0f, -3.0f, -1.43f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {-1.0f, 0.0f, -1.0f, 11.0f, -3.0f, -1.44f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_simple;
};

std::vector<MixedTypedExample>& get_examples_simple_relaxed() {
static std::vector<MixedTypedExample> examples_simple_relaxed = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.0f, 0.0f, -1.0f, 11.0f, -2.0f, -1.44f}}, {1, {-1.0f, 0.0f, 1.0f, 12.0f, -3.0f, -1.43f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {-1.0f, 0.0f, -1.0f, 11.0f, -3.0f, -1.44f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_simple_relaxed;
};

std::vector<MixedTypedExample>& get_examples_simple_quant8() {
static std::vector<MixedTypedExample> examples_simple_quant8 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {{0, {129, 127, 125, 149, 123, 124}}, {1, {125, 127, 129, 151, 121, 124}}},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {{0, {125, 127, 125, 149, 121, 124}}},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_simple_quant8;
};

std::vector<MixedTypedExample>& get_examples_simple_int32() {
static std::vector<MixedTypedExample> examples_simple_int32 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {{0, {1, 0, -1, 11, -2, -1}}, {1, {-1, 0, 1, 12, -3, -1}}},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {{0, {-1, 0, -1, 11, -3, -1}}},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_simple_int32;
};

std::vector<MixedTypedExample>& get_examples_simple_float16() {
static std::vector<MixedTypedExample> examples_simple_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {1.0f, 0.0f, -1.0f, 11.0f, -2.0f, -1.440000057220459f}}, {1, {-1.0f, 0.0f, 1.0f, 12.0f, -3.0f, -1.4299999475479126f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {-1.0f, 0.0f, -1.0f, 11.0f, -3.0f, -1.440000057220459f}}},
}
},
}, // End of an example
};
return examples_simple_float16;
};

std::vector<MixedTypedExample>& get_examples_broadcast() {
static std::vector<MixedTypedExample> examples_broadcast = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.0f, 0.0f, -1.0f, -2.0f, -1.44f, 11.0f}}, {1, {0.5f, 2.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.5f, 0.0f, -1.0f, -2.0f, -1.44f, 2.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_broadcast;
};

std::vector<MixedTypedExample>& get_examples_broadcast_relaxed() {
static std::vector<MixedTypedExample> examples_broadcast_relaxed = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.0f, 0.0f, -1.0f, -2.0f, -1.44f, 11.0f}}, {1, {0.5f, 2.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.5f, 0.0f, -1.0f, -2.0f, -1.44f, 2.0f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_broadcast_relaxed;
};

std::vector<MixedTypedExample>& get_examples_broadcast_quant8() {
static std::vector<MixedTypedExample> examples_broadcast_quant8 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {{0, {129, 127, 125, 123, 124, 149}}, {1, {128, 131}}},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {{0, {128, 127, 125, 123, 124, 131}}},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_broadcast_quant8;
};

std::vector<MixedTypedExample>& get_examples_broadcast_int32() {
static std::vector<MixedTypedExample> examples_broadcast_int32 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {{0, {1, 0, -1, -2, -1, 11}}, {1, {0, 2}}},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {{0, {0, 0, -1, -2, -1, 2}}},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {},
}
},
}, // End of an example
};
return examples_broadcast_int32;
};

std::vector<MixedTypedExample>& get_examples_broadcast_float16() {
static std::vector<MixedTypedExample> examples_broadcast_float16 = {
// Begin of an example
{
.operands = {
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {1.0f, 0.0f, -1.0f, -2.0f, -1.440000057220459f, 11.0f}}, {1, {0.5f, 2.0f}}},
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {},
  // int -> QUANT16_SYMM map
  {},
  // int -> FLOAT16 map
  {{0, {0.5f, 0.0f, -1.0f, -2.0f, -1.440000057220459f, 2.0f}}},
}
},
}, // End of an example
};
return examples_broadcast_float16;
};

