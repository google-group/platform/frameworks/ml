// clang-format off
// Generated file (from: slice.mod.py). Do not edit
#include "../../TestGenerated.h"

namespace slice {
// Generated slice test
#include "generated/examples/slice.example.cpp"
// Generated model constructor
#include "generated/models/slice.model.cpp"
} // namespace slice

TEST_F(GeneratedTests, slice) {
    execute(slice::CreateModel,
            slice::is_ignored,
            slice::examples);
}

TEST_F(GeneratedTests, slice_relaxed) {
    execute(slice::CreateModel_relaxed,
            slice::is_ignored_relaxed,
            slice::examples_relaxed);
}

TEST_F(GeneratedTests, slice_float16) {
    execute(slice::CreateModel_float16,
            slice::is_ignored_float16,
            slice::examples_float16);
}

TEST_F(GeneratedTests, slice_2) {
    execute(slice::CreateModel_2,
            slice::is_ignored_2,
            slice::examples_2);
}

TEST_F(GeneratedTests, slice_relaxed_2) {
    execute(slice::CreateModel_relaxed_2,
            slice::is_ignored_relaxed_2,
            slice::examples_relaxed_2);
}

TEST_F(GeneratedTests, slice_float16_2) {
    execute(slice::CreateModel_float16_2,
            slice::is_ignored_float16_2,
            slice::examples_float16_2);
}

TEST_F(GeneratedTests, slice_3) {
    execute(slice::CreateModel_3,
            slice::is_ignored_3,
            slice::examples_3);
}

TEST_F(GeneratedTests, slice_relaxed_3) {
    execute(slice::CreateModel_relaxed_3,
            slice::is_ignored_relaxed_3,
            slice::examples_relaxed_3);
}

TEST_F(GeneratedTests, slice_float16_3) {
    execute(slice::CreateModel_float16_3,
            slice::is_ignored_float16_3,
            slice::examples_float16_3);
}

TEST_F(GeneratedTests, slice_4) {
    execute(slice::CreateModel_4,
            slice::is_ignored_4,
            slice::examples_4);
}

TEST_F(GeneratedTests, slice_relaxed_4) {
    execute(slice::CreateModel_relaxed_4,
            slice::is_ignored_relaxed_4,
            slice::examples_relaxed_4);
}

TEST_F(GeneratedTests, slice_float16_4) {
    execute(slice::CreateModel_float16_4,
            slice::is_ignored_float16_4,
            slice::examples_float16_4);
}

TEST_F(GeneratedTests, slice_5) {
    execute(slice::CreateModel_5,
            slice::is_ignored_5,
            slice::examples_5);
}

TEST_F(GeneratedTests, slice_relaxed_5) {
    execute(slice::CreateModel_relaxed_5,
            slice::is_ignored_relaxed_5,
            slice::examples_relaxed_5);
}

TEST_F(GeneratedTests, slice_float16_5) {
    execute(slice::CreateModel_float16_5,
            slice::is_ignored_float16_5,
            slice::examples_float16_5);
}

TEST_F(GeneratedTests, slice_6) {
    execute(slice::CreateModel_6,
            slice::is_ignored_6,
            slice::examples_6);
}

TEST_F(GeneratedTests, slice_relaxed_6) {
    execute(slice::CreateModel_relaxed_6,
            slice::is_ignored_relaxed_6,
            slice::examples_relaxed_6);
}

TEST_F(GeneratedTests, slice_float16_6) {
    execute(slice::CreateModel_float16_6,
            slice::is_ignored_float16_6,
            slice::examples_float16_6);
}

TEST_F(GeneratedTests, slice_7) {
    execute(slice::CreateModel_7,
            slice::is_ignored_7,
            slice::examples_7);
}

TEST_F(GeneratedTests, slice_relaxed_7) {
    execute(slice::CreateModel_relaxed_7,
            slice::is_ignored_relaxed_7,
            slice::examples_relaxed_7);
}

TEST_F(GeneratedTests, slice_float16_7) {
    execute(slice::CreateModel_float16_7,
            slice::is_ignored_float16_7,
            slice::examples_float16_7);
}

TEST_F(GeneratedTests, slice_8) {
    execute(slice::CreateModel_8,
            slice::is_ignored_8,
            slice::examples_8);
}

TEST_F(GeneratedTests, slice_relaxed_8) {
    execute(slice::CreateModel_relaxed_8,
            slice::is_ignored_relaxed_8,
            slice::examples_relaxed_8);
}

TEST_F(GeneratedTests, slice_float16_8) {
    execute(slice::CreateModel_float16_8,
            slice::is_ignored_float16_8,
            slice::examples_float16_8);
}

