/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "OperationResolver"

#include "OperationResolver.h"

#include "NeuralNetworks.h"

namespace android {
namespace nn {

// TODO(b/119608412): Find a way to not reference every operation here.
const OperationRegistration* register_ABS();
const OperationRegistration* register_GATHER();

OperationResolver::OperationResolver() {
    registerOperation(register_ABS());
    registerOperation(register_GATHER());
}

const OperationRegistration* OperationResolver::findOperation(OperationType operationType) const {
    auto index = static_cast<int32_t>(operationType);
    if (index < 0 || index >= kNumberOfOperationTypes) {
        return nullptr;
    }
    return mRegistrations[index];
}

void OperationResolver::registerOperation(const OperationRegistration* operationRegistration) {
    CHECK(operationRegistration != nullptr);
    auto index = static_cast<int32_t>(operationRegistration->type);
    CHECK_LE(0, index);
    CHECK_LT(index, kNumberOfOperationTypes);
    CHECK(mRegistrations[index] == nullptr);
    mRegistrations[index] = operationRegistration;
}

}  // namespace nn
}  // namespace android
